#!/bin/bash
cd /var/app
/usr/local/bin/redis-server --daemonize yes | awk '{ print strftime("%Y-%m-%d %H:%M:%S"), $0; fflush(); }' >> /var/app/logs/redis.log 2>&1
cd /var/app/lila-ws
sbt run | awk '{ print strftime("%Y-%m-%d %H:%M:%S"), $0; fflush(); }' >> /var/app/logs/lila-ws.log 2>&1 &
cd /var/app/lila-fishnet
sbt run | awk '{ print strftime("%Y-%m-%d %H:%M:%S"), $0; fflush(); }' >> /var/app/logs/lila-fishnet.log 2>&1 &
cd /var/app/lila
./lila run | awk '{ print strftime("%Y-%m-%d %H:%M:%S"), $0; fflush(); }' >> /var/app/logs/lila.log 2>&1 &
